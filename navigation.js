'use strict';

const navbar_items = [
  { title: 'Home', path: 'index.html' },
  { title: 'Blog', path: 'thoughts/index.html' },
  { title: 'Links', path: 'links.html' },
  { title: 'Docs', path: 'docs.html' },
  { title: 'Videos', path: 'videos.html' }
];


var currentScriptPath = function () {
    var scripts = document.querySelectorAll( 'script[src]' );
    var currentScript = scripts[ scripts.length - 1 ].src;
    var currentScriptChunks = currentScript.split( '/' );
    var currentScriptFile = currentScriptChunks[ currentScriptChunks.length - 1 ];

    return currentScript.replace( currentScriptFile, '' );
}

var htmlPath = window.location.href;
var scriptPath = currentScriptPath();

console.assert(htmlPath.startsWith(scriptPath))

var pathDepth = htmlPath.substring(scriptPath.length).split('/').length - 1;
var pathBase = '';
var i;
for (i = 0; i < pathDepth; i++) {
  pathBase += '../';
}



document.write('<div class="navbar">')
for (i of navbar_items) {
  document.write('<div class="navelem"><a href="' + pathBase + i.path + '">' + i.title + '</a></div>')
  document.write('<div class="navdiv"><p> | </p></div>')
}
document.write('<div class="navelem"><a href="https://gitlab.com/ryukoposting/">GitLab</a></div>');
document.write('<hr>')
document.write('</div>')

