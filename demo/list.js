'use strict';

let MyApp = class extends turck.App {
  constructor() {
    super();
    this.themeColor = "@color/turck_yellow";
    this.textColorOnThemeColor = "@color/black";
    this.buttonColor = "@color/turck_yellow_darker";
    this.buttonPressedColor = "@color/black";
    this.textColorOnBackground = "@color/black";
    this.titleViewUseBackgroundBasedOnThemeColor = true;
  }
  
  onLaunch(launchAction, launchPath, launchParameters) {
    app.showPage(new turck.ui.ListPage({
      items: [
        {
          "type": "category",
          "title": "A category"
        },
        {
          "type": "action",
          "title": "This is an action",
          "click": function () {
            app.showToast("Hello, world!", "Something happened!");
          }
        },
        {
          "type": "yesno",
          "title": "Yes or no?",
          "choices": ["Yeah", "Nah"],
          "key": "yes_or_no",
          "initialValue": true,
          "action": function (isYes) {
            if (isYes)
              app.showToast("yes!", "");
            else
              app.showToast("no!", "");
          }
        },
        {
          "type": "category",
          "title": "Another Category"
        },
        {
          "type": "slider",
          "title": "A slider",
          "key": "a_slider_value",
          "initialValue": 0.5,
          "action": function(value) {
            app.showToast("new value", "the value is " + value);
          }
        },
        {
          "type": "choices",
          "title": "Choose something",
          "key": "some_choice",
          "useSegmentedControl": false,
          "values": [
            "@string/DisplayFormatAscii2",
            "@string/DisplayFormatAscii4",
            "@string/DisplayFormatAscii6",
            "something else"
          ],
          "choices": [
            "Ascii 2",
            "Ascii 4",
            "@string/DisplayFormatAscii6",
            "hmmmmm"
          ],
          "initialSelection": 0,
          "action": function(index, stringValue) {
            app.showToast("You selected choice #" + index, stringValue);
          }
        },
        {
          "type": "choices",
          "title": "Segmented Choice",
          "key": "another_choice",
          "useSegmentedControl": true,
          "values": [
            "1",
            "2",
            "3"
          ],
          "choices": [
            "one",
            "two",
            "three"
          ],
          "initialSelection": 0,
          "action": function(index, stringValue) {
            app.showToast("You selected #" + index, stringValue);
          }
        },
        {
          "type": "numberSpinner",
          "title": "A number spinner",
          "key": "number_spinner_value",
          "min": 3,
          "max": 15,
          "step": 2,
          "firstOnValue": 3,
          "action": function(index) {
            app.showToast("asdf", "value=" + index)
          }
        },
        {
          "type": "action",
          "title": "This is an action with details",
          "click": function () {
            app.showToast("Action with details", "Something happened!");
          },
          // yesno, slider, textedit, choices, and numberSpinner
          // can also have details just like this, with the same semantics:
          "details": "This is a button that does something.",
          "detailsButtonText": "Details",
        }
      ]
    }))
  }
  
};

var app = new MyApp();
