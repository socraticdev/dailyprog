// always do this first:
'use strict';

// declare a new subclass of turck.App, called NissanApp.
let NissanApp = class extends turck.App {
  // create a constructor for your class. We will call this constructor at the
  // end of the script to create your instance of the app.
  constructor() {
    super(); // don't forget to call super!
    // the names of these variables are pretty self-explanatory. When assigning
    // colors, you can either use a hexadecimal code (e.g. "#FF0000" is red), or
    // one of the built-in colors like those being used below. A full list of
    // built-in colors is shown in section 8.5.1
    this.themeColor = "@color/red";
    this.textColorOnThemeColor = "@color/white";
    this.buttonColor = "@color/red";
    this.buttonPressedColor = "@color/black";
    this.textColorOnBackground = "@color/black";
    this.titleViewUseBackgroundBasedOnThemeColor = true;
    
    this.defaultEngineIndex = 0
  }
  // onLaunch gets called when the app first starts up. This function should
  // define the things to be shown on the gadget page ("gadget page" is the
  // in-code name for the app's home screen).
  onLaunch(launchAction, launchPath, launchParameters) {

    // showPage() tells the app to show a page. In this case, we define an
    // instance of the GadgetPage class. Other Page class instances are shown
    // further down in the code.
    app.showPage(new turck.ui.GadgetPage({
      // Show dotted background and curves
      backgroundImage: "@drawable/bg_dots",
      backgroundImageMode: "tile",

      // Some more color variables with self-explanatory names
      underStatusBarColor: "@color/turck_yellow",
      gradientTopColor: "#F0F0F0",
      gradientBottomColor: "#A0A0A0",
      curveColor: "@color/home_circles_stroke",
      belowBottomCircleColor: "@color/bottom_circle_fill",

      // Since this is a gadget page, we must provide a list of "gadgets."
      gadgets:[
        // The first gadget is a turck.ui.Image. This is the Turck logo that
        // appears at the top of the app when it first starts up.
        new turck.ui.Image({
          position: [{type: "parent", ratio: 0.5, at: "center"},
                     {type: "parent", offset: 0, at: "top"}],
          image: "https://ryuk.ooo/demo/nissanlogo.png",
          backgroundColor: "@color/red",
          padding: [-1, 10, -1, 10]
        }),
        // The next gadget is the BL Ident logo, which shows up at the top,
        // offset just a little bit below the Turck logo defined above.
        // This gadget is a StatusImage. This doesn't do anything for a PD67,
        // but on a phone connected to a PD20, it will show the status of the
        // connection.
        new turck.ui.StatusImage({
          position: [{type: "parent", ratio: 1.0, offset: -10, at: "right"},
                     {type: "parent", offset: 48, at: "top"}],
          displayVersionInfoOnTouch: true
        }),
        // This gadget is a TextAndImageButton. The variables that define
        // its appearance should be pretty straightforward.
        new turck.ui.TextAndImageButton({
          position: "curve",
          text: "@string/HomePageScanButton",
          buttonColor: "@color/home_button_text",
          buttonPressedColor: "@color/turck_yellow",
          image: "@drawable/scan_button",
          // when the user taps the button, this function will run. The function
          // defined here will call the function called showScanPage.
          click: function() {
            app.chooseEngine();
          }
        }),

        // Settings button (long press displays additional setings)
        new turck.ui.TextAndImageButton({
          position: "bottom.center",
          text: "@string/HomePageSettingsButton",
          buttonColor: "@color/home_button_text",
          buttonPressedColor: "@color/turck_yellow",
          image: "@drawable/settings_button",
          // the click function works the same as the other two buttons above.
          // in this case, when the button is clicked, the showSettingsPage
          // function is called.
          click: function() { app.showSettingsPage(false); },
          // if you define a longClick function for a button gadget, it will be
          // called if the user presses and holds the button for a couple
          // seconds. the two buttons above could also have longClick functions,
          // if you wanted them.
          longClick: function() { app.showSettingsPage(true); }
        }),
        // The UhfHfSegmentedControl gadget allows the user to switch the PDxx
        // between UHF and HF modes. You can use that 'visibility' parameter
        // on any gadget except this one- the UhfHfSegmentedControl gadget
        // ignores the 'visibility' parameter, and will automatically hide
        // itself if the PDxx doesn't have HF capabilities.
        new turck.ui.UhfHfSegmentedControl({
          position: [{type: "parent", ratio: 0.25, at: "center"},
                     {type: "parent", ratio: 0.32, at: "center"}]
        })
      ]
      
      
    }));
    // always put this at the end of the onLaunch function!
    app.handleLaunchParameters(launchAction, launchPath, launchParameters);
  }
  
  chooseEngine() {
    app.showChoices(
      "Choose an Engine",
      ["VQ30", "VQ37", "SR20"],
      function(choiceIndex, choice) { // selected a thing
        app.defaultEngineIndex = choiceIndex;
      },
      app.defaultEngineIndex,
      "", // message
      "", // actionButtonTitle
      true, // canCancel
      function(choiceIngex, choice, completion) { // confirmationCompletion
        app.chooseItemToScan(choice);
        completion();
      },
      function() { // cancelCompletion
        
      }
    );
  }
  
  chooseItemToScan(engine) {
    app.showMenu("Choose Item to Scan", [
      "3-Digit Model Code",
      function() {
        var title = engine + " 3-Digit Model Code";
        app.showReadWritePage(title, "epc", 4, 4, "text", "UTF-8");
      },
      
      "Serial Number",
      function() {
        var title = engine + " Serial Number";
        app.showReadWritePage(title, "epc", 8, 8, "text", "UTF-8");
      },
      
      "2-Digit Model Code",
      function() {
        var title = engine + " 2-Digit Model Code";
        app.showReadWritePage(title, "tid", 0, 2, "hex");
      },
      
      "10-Digit Model Code",
      function() {
        var title = engine + " 10-Digit Model Code";
        app.showReadWritePage(title, "epc", 0, 10, "decimal");
      }
    ],
    function() {
      // this runs if the menu gets canceled
    });
  }

  // showSettingsPage was called by some of the 'click' functions on the gadget
  // page. There is not much to customize with the settings page. It is set up
  // for you.
  showSettingsPage(showAdvancedSettings) {
    app.showPage(new turck.ui.SettingsPage({
      titleText: "@string/HomePageSettingsButton",
      showAdvancedSettings: showAdvancedSettings
    }));
  }

  // the showReadWritePage function shows the read write page. If an epc is
  // passed into the function, it will load that tag automatically.
  showReadWritePage(title, domain, startByte, numBytes, format, charset, epc) {
    var rwPage =
      new turck.ui.ReadWritePage({
        titleText: title,
        epc: epc,
        
        tagFound: function(tag) {
          return true;
        },
        
        onPD67RightButtonPress: function() {
          if (!this.isScanning) {
           this.writeTag();
          }
        },
        
        onPD67LeftButtonPress: function() {
          if (!this.isScanning) {
           this.readTag();
          }
        },
        
        onPD67TriggerButtonPress: function() {
          if (!this.isScanning) {
           this.readTag();
          }
        }
      });
    
    if (!(domain === null)) {
      rwPage.domain = domain;
    }
    
    if (!(startByte === null)) {
      rwPage.startByte = startByte;
    }
    
    if (!(numBytes === null)) {
      rwPage.numBytes = numBytes;
    }
    
    if (!(format === null)) {
      rwPage.format = format;
    }
    
    if (!(charset === null)) {
      rwPage.charset = charset;
    }
    
    app.showPage(rwPage);
  }
}

// Create the app object. very important!
var app = new NissanApp();
